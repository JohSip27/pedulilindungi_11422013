<?php
  session_start();
  if(!isset($_SESSION['username'])) {
    header("location:login.php");
    exit;
  }

  require 'config.php';
  ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tentang</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  </head>
  <body class="bg-info">
  <nav class="navbar navbar-expand-lg bg-secondary">
  <div class="container-fluid">
    <a href="home.php"><img src="Logo1.jpg" alt="" width="100px;" height="60px;"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-light" aria-current="page" href="faskes.php">Faskes Toba</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light" href="checkin_als.php">Check In</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light" href="vaksin.php">Vaksin</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="tentang.php">Tentang</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="profil.php">Profil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="logout.php">Keluar</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <div class="container mt-5">
    <div class="card">
        <div class="card-body">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="home.php" class="text-decoration-none">Home</a></li>
                <li class="breadcrumb-item">Tentang</li>
            </ol>
        </nav>
        <div class="row">
    <div class="col-4">
      <h5 class="text-secondary">Tentang</h5>
      <h3 class="text-info">Apa itu PeduliLindungi</h3>
    </div>
    <div class="col-8">
        <p>PeduliLindungi adalah aplikasi yang dikembangkan untuk membantu instansi       pemerintah terkait dalam melakukan pelacakan untuk menghentikan penyebaran         Coronavirus Disease (COVID-19).
        Aplikasi ini mengandalkan partisipasi masyarakat untuk saling membagikan data lokasinya saat bepergian agar penelusuran riwayat kontak dengan penderita COVID-19 dapat dilakukan.
        Pengguna aplikasi ini juga akan mendapatkan notifikasi jika berada di keramaian atau berada di zona merah, yaitu area atau kelurahan yang sudah terdata bahwa ada orang yang terinfeksi COVID-19 positif atau ada Pasien Dalam Pengawasan.</p>
    </div>
    <hr>
    <div class="row">
    <div class="col-4">
      <h5 class="text-secondary">Tentang</h5>
      <h3 class="text-info">Vaksinasi COVID-19</h3>
    </div>
    <div class="col-8">
        <p>Pada tahap awal, vaksinasi Covid-19 sudah berhasil diberikan kepada seluruh tenaga kesahatan, asisten tenaga kesehatan, dan mahasiswa yang menjalankan pendidikan profesi kedokteran yang bekerja pada fasilitas pelayanan kesehatan.
        Vaksin tahap kedua juga sudah diberikan kepada lansia, pekerja sektor esensial, dan guru.
        Pemerataan vaksinasi hingga saat ini dilanjutkan untuk masyarakat umum dan terus berjalan hingga berhasil menjangkau seluruh warga negara Indonesia dan warga negara asing yang bertempat tinggal di Indonesia.Harapannya dengan upaya pemerataan vaksinasi ini, Indonesia dapat segera bangkit dan terbebas dari penyebaran virus Covid-19.</p>
    </div>
    <hr>
    <div class="row">
    <div class="col">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/2hmI15CajEM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
    <div class="col">
      <h3 class="text-info">Bagaimana PeduliLindungi bekerja?</h3>
      <p>Pada saat Anda mengunduh PeduliLindungi, sistem akan meminta persetujuan Anda untuk mengaktifkan data lokasi. Dengan kondisi lokasi aktif, maka secara berkala aplikasi akan melakukan identifikasi lokasi Anda serta memberikan informasi terkait keramaian dan zonasi penyebaran COVID-19.
      Hasil tracing ini akan memudahkan pemerintah untuk mengidentifikasi siapa saja yang perlu mendapat penanganan lebih lanjut agar penghentian penyebaran COVID-19 dapat dilakukan. Sehingga, semakin banyak partisipasi masyarakat yang menggunakan aplikasi ini, akan semakin membantu pemerintah dalam melakukan tracing dan tracking.
      PeduliLindungi sangat memperhatikan kerahasiaan pribadi Anda. Data Anda disimpan aman dalam format terenkripsi dan tidak akan dibagikan kepada orang lain. Data Anda hanya akan diakses bila Anda dalam risiko tertular COVID-19 dan perlu segera dihubungi oleh petugas kesehatan.</p>
    </div>
  </div>
  </div>
        </div>
    </div>
  </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>
</html>