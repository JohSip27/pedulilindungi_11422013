<?php
$nama = "";
$password = "";
$nama_err = "";
$password_err="";


if(isset($_POST['submit'])) {
  if(empty(trim($_POST['username']))) {
    $nama_err = "Nama harus diisi";
  } else {
    $_nama = $_POST['username'];
    header("Location:register_process.php");
  }

  if(empty(trim($_POST['password']))) {
    $password_err = "Password harus diisi";
  } else {
    $_password = $_POST['passsword'];
    header("Location:register_process.php");
  }

}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Daftar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  </head>
  <body class="bg-info">
    <div class="container mt-5">
        <div class="card">
            <div class="card-body">
            <h3><b>Daftar</b></h3>
            <hr>
                <form action="register_process.php" method="post">
                    <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">Username</label>
                  <input type="text" class="form-control" id="exampleFormControlInput1" name="username">
                </div>
                <?php echo $nama_err;?>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">Password</label>
                  <input type="password" class="form-control" id="exampleFormControlInput1" name="password">
                </div>
                <?php echo $password_err;?>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">Konfirmasi Password</label>
                  <input type="password" class="form-control" id="exampleFormControlInput1" name="password">
                </div>
                <button class="btn btn-primary" name="submit">Masuk</button>
                <span>Sudah Memiliki Akun? | <a href="login.php" class="text-decoration-none">Masuk Disini</a></span>
                </form>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    </div>
  </body>
</html>

<?php



?>